const Nightmare = require('nightmare');
const nightmare = Nightmare({show: true});


nightmare
    // .inject('js', '../node_modules/jquery/dist/jquery.js')
    // .on ('console', console.log.bind (console))
    .goto('https://www.google.com/maps')
    .insert('#searchboxinput', 'eat farrington london')
    .click('#searchbox-searchbutton')
    .wait(10000)
    .evaluate(function() {
            console.log("revaluating results now");
            let searchResults = [];

            const results =  document.querySelectorAll('.section-result');
            console.log("selected results " , results.length);
            results.forEach(function(result) {
                console.log("nightmare" , nightmare);
                nightmare
                .click(result.className)
                .wait(10000)
                .evaluate(function(){
                    const name = document.querySelector('#pane > div > div.widget-pane-content.scrollable-y > div > div > div.section-hero-header-title > div.section-hero-header-title-top-container > div.section-hero-header-title-description > div:nth-child(1) > h1 > span:nth-child(1)')
                    console.log("Name " , name);
                    searchResults.push({name});
                })
            });
            return searchResults;
    })
    .end()
    .then(function(result) {
        console.log(result);
            result.forEach(function(r) {
                    
            })
    })
    .catch(function(e)  {
            console.log(e);
    });