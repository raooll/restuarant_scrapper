const commandLineArgs = require("command-line-args");
const map_scrape = require("./google_map_pups")
const d3 = require("d3-dsv");
const _ = require("lodash");
var fs = require('fs');

var verbose = false;
var query = "";
var pages = 1;
var output_file = null;
var headless = false;
var devtools = false;

const optionDefinitions = [
  { name: "verbose", alias: "v", type: Boolean, defaultValue: false },
  {
    name: "query",
    alias: "q",
    type: String,
    multiple: true,
    defaultOption: true,
  },
  { name: "pages", alias: "p", type: Number, defaultValue: 1 },
  { name: "output_file", alias: "o", type: String },
  { name: "headless", alias: "h", type: Boolean, defaultValue: false },
  { name: "devtools", alias: "t", type: Boolean, defaultValue: false },
];

const options = commandLineArgs(optionDefinitions);

query = options.query.join(" ");
output_file = options.output_file || `${options.query.join("_")}.csv`;
verbose = options.verbose;
pages = options.pages;

console.log("options", options);


const trimObject = (o) => {
    return _.reduce(o, function(acc, v, k) {
      return acc[_.trim(k)] = _.trim(v), acc;
    }, {});
  }

(async () => {
    var results = await  map_scrape(query, pages, verbose, headless, devtools);
    console.log("scrapping item count ", results.length);
    results = results.map(trimObject);
    csv_results = d3.csvFormat(results);

    fs.writeFile(output_file, csv_results, function (err) {
        if (err) throw err;
        console.log('Saved!');
      });
})().then(console.log);
