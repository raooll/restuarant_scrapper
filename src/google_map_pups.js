const puppeteer = require("puppeteer");
const Promise = require("bluebird");
var async = require("async");
const _ = require("lodash");

const scrape_one_item = () => {
  return new Promise(function (resolve, reject) {
    var data = {category: "R"};

    brand_name_element = document.querySelector(
      "#pane > div > div.widget-pane-content.scrollable-y > div > div > div.section-hero-header-title > div.section-hero-header-title-top-container > div.section-hero-header-title-description > div:nth-child(1) > h1 > span:nth-child(1)"
    );
    if (brand_name_element) {
      data["brand_name"] = brand_name_element.textContent;
    }

    site_address = document.querySelector('[data-item-id="address"]');
    if (site_address) {
      data["site_address"] = site_address.textContent;
    }

    brand_phone = document.querySelector('[data-tooltip="Copy phone number"]');
    if (brand_phone) {
      data["brand_phone"] = brand_phone.textContent;
    }

    brand_website = document.querySelector('[data-tooltip="Open website"]');
    if (brand_website) {
      data["brand_website"] = brand_website.textContent;
    }

    status = document.querySelector(
      "#pane > div > div.widget-pane-content.scrollable-y > div > div > div.cX2WmPgCkHi__root.gm2-body-2.cX2WmPgCkHi__dense > div.cX2WmPgCkHi__summary-line.cX2WmPgCkHi__clickable > div > div.cX2WmPgCkHi__primary-text > span.cX2WmPgCkHi__section-info-hour-text > span.cX2WmPgCkHi__section-info-text.cX2WmPgCkHi__red"
    );
    if (status) {
      data["status"] = status.textContent;
    }

    document
      .querySelector(
        "#pane > div > div.widget-pane-content.scrollable-y > div > div > button > span"
      )
      .click();

    resolve(data);
  });
};

const scrape_page = async (page) => {
  await page.waitFor(4 * 1000);
  await page.waitForSelector(".section-result", { visible: true });
  var elems = await page.$$(".section-result");
  console.log(`Found ${elems.length} items to scrape`);

  const page_results = [];
  for (i = 0; i < 20; i++) {
    var elems = await page.$$(".section-result");

    // Figure out a better way later
    if (elems.length < i) {
      console.log("Too few results returning");
    }
    var elem = elems[i];
    elem.click();

    page.waitForSelector(".section-hero-header-title-title", { visible: true });

    //Skip if temporarily closed to be fixed
    const elemText = elem.innerText;
    if (elemText && elem.innerText.includes("Temporarily closed")) {
      continue;
    }

    await page.waitForSelector(
      "#pane > div > div.widget-pane-content.scrollable-y > div > div > button > span"
    );
    const page_data = await page.evaluate(scrape_one_item);

    await page.waitForSelector(".section-result", { visible: true });
    page_results.push(page_data);
  }

  console.log("scraped result on a page :- ", page_results.length);
  return page_results;
};

const start_scrape = async (query, pages, verbose, headless, devtools) => {
  const browser = await puppeteer.launch({
    headless: headless,
    devtools: devtools,
  });
  const page = (await browser.pages())[0];

  await page.goto("https://www.google.com/maps");

  if (verbose) {
    page.on("console", (msg) => console.log("PAGE LOG:", msg.text()));
  }

  const search_box_selector = "#searchboxinput";
  const search_button_selector = "#searchbox-searchbutton";

  await page.click(search_box_selector);
  await page.keyboard.type(query);

  await page.click(search_button_selector);

  return new Promise(async (resolve, reject) => {
    Promise.mapSeries(_.range(pages), async (p) => {
      console.log(`Scrapping page number ${1 + p} of total ${pages}`);
      const a_page_results = await scrape_page(page);

      console.log("Going to next page");
      const next_page_selector = '[jsaction="pane.paginationSection.nextPage"]';
      await page.waitForSelector(next_page_selector, { visible: true });
      await page.click(next_page_selector);

      await page.waitFor(10 * 1000);
      return a_page_results;
    }).then(async (results) => {
        await browser.close();
      resolve(_.flatten(results));
    });
  }).catch(async (err) => {
    console.log("Opps error occured");
    await browser.close();
  });

};

module.exports = start_scrape;
