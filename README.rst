============
Instruction :-
============

Clone the repo:
***************
    git clone https://gitlab.com/raooll/restuarant_scrapper/

Install dependencies:
***********************


    cd restuarant_scrapper
    
    npm install


Scrape:
********
    npm run scrape -- "search query" [... options]

Options:
**********
    
    +---------------+---+-----------------------------+---------------------------------------------------------+
    | verbose       | -v|   default: false            | #verbose                                                |
    +---------------+---+-----------------------------+---------------------------------------------------------+
    | pages         | -p|   default: 1                | #number of pages to scrape, each page has max 20 entries|
    +---------------+---+-----------------------------+---------------------------------------------------------+
    | headless      | -h|   default: false            | #run the scrape in a headless browser                   |
    +---------------+---+-----------------------------+---------------------------------------------------------+
    | devtools      | -t|  default: false             | #open the devtools in the browser                       |
    +---------------+---+-----------------------------+---------------------------------------------------------+
    | query         | -q|   default: none             | #the search query                                       |
    +---------------+---+-----------------------------+---------------------------------------------------------+
    | output_file   | -o| default: generate from query| #the filename to save data                              |
    +---------------+---+-----------------------------+---------------------------------------------------------+


Note:
*********
The scrapper might crash , do not worry just run it again.